import {fetchPeople} from "../DataProvider/DataProvider";
import * as Actions from "../constants/actions"

export const actionPeople = () => dispatch => {
    fetchPeople().then(data => {
        return dispatch({type: Actions.PEOPLE_LOAD_COMPLETED, data})
    }).catch(error => {
        return dispatch({type: Actions.PEOPLE_LOAD_FAILED, errorMessage: error})
    })
    //debugger;
}

// export const actionPeople = async () => dispatch => {
//     try {
//         const data = await fetchPeople();
//         return dispatch({type: Actions.PEOPLE_LOAD_COMPLETED, data})
//     } catch (error) {
//         return dispatch({type: Actions.PEOPLE_LOAD_FAILED, errorMessage: error})
//     }
// }
