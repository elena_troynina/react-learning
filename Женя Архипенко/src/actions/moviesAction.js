import {fetchMovies} from "../DataProvider/DataProvider";
import * as Actions from "../constants/actions"

export const actionMovies = () => dispatch => {
    fetchMovies().then(data => {
        return dispatch({type: Actions.MOVIES_LOAD_COMPLETED, data})
    }).catch(error => {
        return dispatch({type: Actions.MOVIES_LOAD_FAILED, errorMessage: error})
    })
    //debugger;
}

// export const actionMovies = async () => dispatch => {
//     try {
//         const data = await fetchMovies();
//         return dispatch({type: Actions.MOVIES_LOAD_COMPLETED, data})
//     } catch (error) {
//         return dispatch({type: Actions.MOVIES_LOAD_FAILED, errorMessage: error})
//     }
// }
