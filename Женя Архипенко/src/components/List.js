import React from "react";
import PropTypes from 'prop-types';

const List = props => (
    <ul>
        {props.items.map((item) => (
            <li key={item.key}>
                <span>
                    {item.text}
                </span>
            </li>
        )
    )}
    </ul>
);

List.propTypes = {
    items: PropTypes.array.isRequired
}

export default List;