import React, {Component} from 'react';
import {fetchMovies} from '../DataProvider/DataProvider';
import List from './List';
import {connect} from "react-redux";
import {actionMovies} from '../actions/moviesAction';
import {moviesReducer} from "../reducers/moviesReducer";
import {actionPeople} from "../actions/peopleAction";

class MoviesContainer extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            isLoading: false
        };
    }

    componentDidMount() {
        const {dispatchFetchMovies} = this.props
        dispatchFetchMovies()
    }

    // render(){
    //     const {data: {movies, isLoading}} = this.props;
    //
    //     const list = !isLoading && movies && movies.length && <List items={movies}/>
    //     const loading = isLoading && <div>Loading...</div>
    //     return (
    //         <div>
    //             {list}
    //             {loading}
    //         </div>
    //     );
    // }
    render(){
        const {data: {movies, isLoading}} = this.props;

        if(!isLoading && movies && movies.length) {
            return (
                <List items={movies}/>
            );
        }

        return (
            <div>Loading...</div>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.moviesReducer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchFetchMovies: () => {
            dispatch(actionMovies())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MoviesContainer)







//With fetch
// componentDidMount() {
//     fetch('https://swapi.co/api/films/')
//     //fetch('https://facebook.github.io/react-native/movies.json')
//         .then((response) => response.json())
//         .then((response) => this.setState({films: response.results}));
//     //.then((response) => this.setState({films: response.movies}));
// }

//With axios
//     getMovies(item) {
//         axios.get(item)
//             .then(response => {
//                 console.log(response);
//                 this.setState({films: response.data.results});
//             })
//     };
