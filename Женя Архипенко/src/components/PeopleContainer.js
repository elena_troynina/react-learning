import React, {Component} from 'react';
import {fetchPeople} from '../DataProvider/DataProvider';
import List from "./List";
import {actionPeople} from '../actions/peopleAction';
import connect from "react-redux/es/connect/connect";
import {peopleReducer} from "../reducers/peopleReducer";

class PeopleContainer extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            isLoading: false
        };
    }

    componentDidMount() {
        // const {dispatch} = this.props
        // dispatch(actionPeople())
        const {dispatchFetchPeople} = this.props
        dispatchFetchPeople()
    }

    // render(){
    //     const {data: {people, isLoading}} = this.props;
    //
    //     const list = !isLoading && people && people.length && <List items={people}/>
    //     const loading = isLoading && <div>Loading...</div>
    //         return (
    //             <div>
    //                 {list}
    //                 {loading}
    //             </div>
    //         );
    //     }

    render(){
        const {data: {people, isLoading}} = this.props;

        if(!isLoading && people && people.length) {
            return (
                <List items={people}/>
            );
        }

        return (
            <div>Loading...</div>
        );
    }
}

const mapStateToProps = state => {
    return {
        data: state.peopleReducer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchFetchPeople: () => {
            dispatch(actionPeople())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PeopleContainer)
