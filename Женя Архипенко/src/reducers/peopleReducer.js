import * as Actions from "../constants/actions"

const initialState = {
    people: [],
    isLoading: false,
    errorMessage: ''
};

export const peopleReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.PEOPLE_LOAD_STARTED:
            return { ...state, isLoading: true}
        case Actions.PEOPLE_LOAD_FAILED:
            return { ...state, isLoading: false, errorMessage: 'Error load'}
        case Actions.PEOPLE_LOAD_COMPLETED:
            return { ...state, people:action.data, isLoading: false}
        default:
            //debugger;
            return state
    }
}