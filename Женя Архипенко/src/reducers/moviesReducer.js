import * as Actions from "../constants/actions"


const initialState = {
    movies: [],
    isLoading: false,
    errorMessage: ''
};

export const moviesReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.MOVIES_LOAD_STARTED:
            return { ...state, isLoading: true}
        case Actions.MOVIES_LOAD_FAILED:
            return { ...state, isLoading: false, errorMessage: 'Error load'}
        case Actions.MOVIES_LOAD_COMPLETED:
            return { ...state, movies:action.data, isLoading: false}
        default:
            //debugger;
            return state
    }
}