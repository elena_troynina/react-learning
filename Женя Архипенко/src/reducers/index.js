import { combineReducers } from 'redux'
import {peopleReducer} from '../reducers/peopleReducer'
import {moviesReducer} from '../reducers/moviesReducer'

export const rootReducer = combineReducers({
    peopleReducer,
    moviesReducer
})

