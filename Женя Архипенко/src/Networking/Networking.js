import axios from 'axios';

export const doGetRequest = async url => {
    try {
        const response = await axios.get(url)
        if (response.status === 200) {
            return {data: response.data}
        } else {
            //TODO
            return {error: response.error}
        }
    } catch (error) {
        //TODO
        return {error}
    }
}