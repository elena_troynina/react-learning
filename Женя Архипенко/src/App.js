import React, {Component} from 'react';
import './App.css';
import MoviesContainer from './components/MoviesContainer';
import PeopleContainer from './components/PeopleContainer';

class App extends Component {
    render(){
        return (
            <React.Fragment>
            <MoviesContainer />
            <PeopleContainer />
            </React.Fragment>
        );
    }
}

export default App;