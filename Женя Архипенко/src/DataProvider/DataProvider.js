import {doGetRequest} from "../Networking/Networking";

const getData = async (url) => {
    const {data, error} = await doGetRequest(url)
    //debugger
    //TODO
    if(error) {
        return error.message;
    }
    return data.results;
};

export const fetchMovies = async () => {
    const result = await getData('https://swapi.co/api/films/')
    const items = result.map((item) => {return{text: item.title, key: item.id}});
    return items;

};


export const fetchPeople = async () => {
    const result = await getData('https://swapi.co/api/people/')
    const items = result.map((item) => {return{text: item.name, key: item.name}});
    return items;
};
