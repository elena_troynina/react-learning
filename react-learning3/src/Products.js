/**
 * Products component *
 */
import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';


const Products = ({ match }) => {
    return(
        <div>
            <ul>
                <li><Link to={`${match.url}/shoes`}>Shoes</Link></li>
                <li><Link to={`${match.url}/boots`}>Boots</Link></li>
                <li><Link to={`${match.url}/footwear`}>Footwear</Link></li>
                <li><Link to={`${match.url}/product1`}>Product1</Link></li>
            </ul>
            <Switch>
                <Route path={`${match.path}/:name`}
                       render= {({match}) =>( <div> <h3> {match.params.name} </h3></div>)}/>
            </Switch>
    </div>)
}

export default Products;