import React from 'react';
import './App.css';
import { Link, Route, Switch } from 'react-router-dom';
import Products from './Products';

/* Home component */
const Home = () => (
    <div>
      <h2>Home</h2>
    </div>
)

/* Cart component */
const Cart = () => (
    <div>
      <h2>Cart</h2>
    </div>
)

/* Product Product1 component*/
const Product1 = () => (
    <div>
        <h2>Product1</h2>
    </div>
)


const MainMenu = () => (
    <div>
      <nav className="navbar navbar-light">
        <ul className="nav navbar-nav">
          <li><Link to="/">Homes</Link></li>
          <li><Link to="/cart">Cart</Link></li>
          <li><Link to="/products">Products</Link></li>
          <li><Link to="/products/product1">Product1</Link></li>
        </ul>
      </nav>
    </div>
)

class App extends React.Component {
    render() {
        return (
            <div>
              <MainMenu />
                <Switch>
                  <Route exact path="/" component={Home}/>
                  <Route path="/cart" component={Cart}/>
                    <Route exact path="/products" component={Products}/>
                    <Route path="/products/product1" component={Product1}/>
                </Switch>
            </div>
        )
    }
}

export default App;
