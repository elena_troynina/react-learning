/**
 * Movies component *
 */
import React from 'react';

class List extends React.Component {
    // this.props.log
    render() {
        const {items, title} = this.props;

        return (
            <div>
                <h1>{title}</h1>
                <br />
                <ul>
                    {items.map(item =>
                        <li key={item}>
                            {item}
                        </li>
                    )}
                </ul>
            </div>
        );
    }
}

export default List;
