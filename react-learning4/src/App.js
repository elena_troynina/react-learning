import React, { Component } from 'react';
import './App.css';
import MoviesContainer from './MoviesContainer';
import PeopleContainer from './PeopleContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <MoviesContainer />
        <PeopleContainer />
      </div>
    );
  }
}

export default App;
