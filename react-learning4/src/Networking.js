/**
 * Networking *
 */
import axios from 'axios';

const getDataWithUrl = async url => {
    try {
        const response = await axios.get(url);
        return response.data.results;
    } catch (error) {
        console.log(error);
        return error;
    }
};

export const getMovies = () => {
    return getDataWithUrl('https://swapi.co/api/films/');
};

export const getPeople = () => {
    return getDataWithUrl('https://swapi.co/api/people/');
};
