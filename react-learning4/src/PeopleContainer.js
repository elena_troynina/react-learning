/**
 * PeopleContainer component *
 */
import React from 'react';
import {getPeople}  from './Networking';
import List from './List';

class PeopleContainer extends React.Component {
    state = {
        data: []
    };

    componentDidMount() {
        getPeople()
            .then(obj => {
                const people = obj.map(obj => obj.name);
                this.setState({data:people});
            });
    }

    render() {
        return <List items={this.state.data} title={'People'}/>;
    }
}

export default PeopleContainer;
