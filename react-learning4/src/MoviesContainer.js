/**
 * MoviesContainer component *
 */
import React from 'react';
import {getMovies}  from './Networking';
import List from './List';

class MoviesContainer extends React.Component {
    state = {
        data: []
    };

    componentDidMount() {
        getMovies()
            .then(obj => {
                const films = obj.map(obj => obj.title);
                    this.setState({data:films});
            });
    }

    render() {
        return <List items={this.state.data} title={'Movies'}/>;
    }
}

export default MoviesContainer;