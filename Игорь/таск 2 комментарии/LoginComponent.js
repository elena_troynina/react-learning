/**
 * Created by user on 03.12.18.
 */
import React from 'react'

class LoginComponent extends React.Component {
  state = {
    userName: '',
    password: '',
    formErrors: { userName: '', password: '' },
    userNameValid: false,
    passwordValid: false,
    formValid: false
  }

  componentWillMount() {
    // я хочу спать и не проверял, но по логике
    // вот тут впервые присваивается значение this.initialState
    // в контексте этого приложения и сценария его использования этот метод никогда не вызывается
    // нужно или избавиться от инишиал стейт вообще или использовать его на полную, объявив его над этим классом
    // делать этого всего не надо, это просто что б ты поняла к чему я
    this.initialState = this.state
  }

  // этот метод вызывает this.setState
  // и этот же метод еще раз вызывает this.setState,
  // который вызывает validateField как коллбек,
  // validateForm как коллбек,
  // а (почти)каждый вызов this.setState влечет за собой вызов render
  // а лишний (не нужный) рендер это плохо
  // то есть - на одно изменение инпута формы, у тебя 3 рендера, 1 нужный, а еще 2 - нет
  // https://www.dropbox.com/s/hnno4wlek50aej1/render.mov?dl=0
  // задание со звездочкой - избавится от этого
  // если не осилишь, поговори с Мирющенко, у него была та же "проблема"
  // еще раз, это мелочи и комп потянет и все такое, но почти все проблемы и оптимизации реакты направленны на избавление от перерендера
  handleChange = event => {
    console.log('handleChange')
    const {
      target: { value, name }
    } = event

    this.setState({ [name]: value }, () => {
      this.validateField(name, value)
    })
  }

  handleSubmit = event => {
    alert('A name was submitted: ' + this.state.userName)

    this.setState(this.initialState)

    // зачем этот код?
    // this.setState(function(){
    //     return {
    //         state: this.initialState
    //     }
    // });

    event.preventDefault()
  }

  validateField(fieldName, value) {
    // ты ж помнишь, что нельзя изменять стейт напрямую?
    // когда я говорил, что не стоит сейчас хранить formErrors с еще одиним уровнем вложенности
    // я боялся, что ты будешь мутировать стейт на прямую (смотри ниже)
    let fieldValidationErrors = this.state.formErrors
    let userNameValid = this.state.userNameValid
    let passwordValid = this.state.passwordValid

    switch (fieldName) {
      case 'userName':
        userNameValid = value.length >= 4
        // вот тут ты мутируешь стейт напрямую
        fieldValidationErrors.userName = userNameValid ? '' : ' is too short'
        break
      case 'password':
        passwordValid = value.length >= 4
        // вот тут ты мутируешь стейт напрямую
        fieldValidationErrors.password = passwordValid ? '' : ' is too short'
        break
      default:
        break
    }

    // хоть ты и мутируешь стейт напрямую, в этот раз тебе повезло
    // и (вроде бы) это не добавило ошибок
    // почему нельзя мутировать стейт на прямую?

    this.setState(
      {
        formErrors: fieldValidationErrors,
        userNameValid: userNameValid,
        passwordValid: passwordValid
      },
      this.validateForm
    )
  }

  validateForm() {
    this.setState({
      formValid: this.state.userNameValid && this.state.passwordValid
    })
  }

  render() {
    console.log('render')
    return (
      <div className="login-card">
        <h1>Log-in</h1>
        <br />
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            value={this.state.userName}
            name="userName"
            placeholder="Name"
            onChange={this.handleChange}
            className={`form-input ${
              this.state.formErrors.userName ? 'has-error ' : ''
            }`}
          />
          <input
            type="password"
            value={this.state.password}
            name="password"
            placeholder="Password"
            onChange={this.handleChange}
            className={`form-input ${
              this.state.formErrors.password ? 'has-error ' : ''
            }`}
          />
          <input
            type="submit"
            name="login"
            className="login login-submit"
            disabled={!this.state.formValid}
            value="Login"
          />
        </form>
      </div>
    )
  }
}

export default LoginComponent
