import axios from 'axios'

export const fetchFilms = async () => {
  return await getDataWithUrl('https://swapi.co/api/films/')
}

export const fetchPeople = async () => {
  return await getDataWithUrl('https://swapi.co/api/people/')
}

const getDataWithUrl = async url => {
  try {
    const response = await axios.get(url)
    return response.data.results
  } catch (error) {
    return error
  }
}
