import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import '../../App.css'

class StateSwitcherComponent extends PureComponent {
  render() {
    const { currentAction, onNextStateClicked } = this.props
    return (
      <div>
        <button
          className="button"
          onClick={() => {
            onNextStateClicked()
          }}
        >
          Next state
        </button>
        {currentAction}
      </div>
    )
  }
}

StateSwitcherComponent.propTypes = {
  currentAction: PropTypes.string.isRequired,
  onNextStateClicked: PropTypes.func.isRequired
}

export default StateSwitcherComponent
