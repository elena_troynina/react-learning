import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import LightComponent from './LightComponent'
import '../../App.css'

class TrafficLightComponent extends PureComponent {
  render() {
    const { lights, colors } = this.props

    const lightComponents = lights.map((mode, index) => {
      const color = colors[index]
      return <LightComponent key={index} mode={mode} color={color} />
    })

    return <div className="trafficLightContainer">{lightComponents}</div>
  }
}

TrafficLightComponent.propTypes = {
  lights: PropTypes.array.isRequired,
  colors: PropTypes.array.isRequired
}

export default TrafficLightComponent
