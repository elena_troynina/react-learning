import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ON, OFF, BLINKING } from '../../constants/LightMode'
import '../../App.css'

class LightComponent extends Component {
  constructor(props) {
    super(props)

    this.state = { isBlinking: false, isLightOn: false }
  }

  componentDidMount() {
    this.updateLight(this.props)
  }

  componentWillReceiveProps(props) {
    this.updateLight(props)
  }

  updateLight(props) {
    const { mode } = props
    let isBlinking
    let isLightOn
    switch (mode) {
      case ON:
        isLightOn = true
        isBlinking = false
        break
      case OFF:
        isLightOn = false
        isBlinking = false
        break
      case BLINKING:
        isLightOn = true
        isBlinking = true
        break

      default:
        break
    }

    this.setState({
      isLightOn,
      isBlinking
    })

    if (isBlinking) {
      this.intervalId = setInterval(() => {
        this.switchLight()
      }, 500)
    } else {
      clearInterval(this.intervalId)
    }
  }

  switchLight() {
    const isLightOn = !this.state.isLightOn
    this.setState({ isLightOn })
  }

  render() {
    const { color } = this.props
    const { isLightOn } = this.state
    const disabledClass = !isLightOn ? 'disabled' : ''
    const className = `circle ${color} ${disabledClass}`
    return <div className={className} />
  }
}

LightComponent.propTypes = {
  color: PropTypes.string.isRequired,
  mode: PropTypes.number.isRequired
}

export default LightComponent
