import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import TrafficLightComponent from '../components/TrafficLight/TrafficLightComponent'

class TrafficLightContainer extends PureComponent {
  render() {
    const { carTrafficLight } = this.props

    return (
      <div className="xing">
        <TrafficLightComponent {...carTrafficLight} />
      </div>
    )
  }
}

TrafficLightContainer.propTypes = {
  carTrafficLight: PropTypes.shape({
    lights: PropTypes.array.isRequired,
    colors: PropTypes.array.isRequired
  })
}

// достаем из redux store те ключи, в которых заинтресованы
const mapStateToProps = state => {
  return {
    carTrafficLight: state.carTrafficLight
  }
}

// Функция connect оборачивает в себя React компоненту
// пробрасывая в props ссылки на redux store (см. mapStateToProps)
export default connect(mapStateToProps)(TrafficLightContainer)
