import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import StateSwitcherComponent from '../components/StateSwitcherComponent/StateSwitcherComponent'
import * as Actions from '../constants/Actions'
import '../App.css'

class StateSwitcherContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentIndex: -1,
      currentAction: '',
      actions: [
        Actions.TRAFFIC_DISALLOWED,
        Actions.TRAFFIC_BEFORE_ALLOWED,
        Actions.TRAFFIC_ALLOWED,
        Actions.TRAFFIC_AFTER_ALLOWED,
        Actions.TRAFFIC_BEFORE_DISALLOWED
      ]
    }
  }

  componentDidMount() {
    const { changeTrafficState } = this.props
    changeTrafficState(Actions.TRAFFIC_OUT_OF_SERVICE)
  }

  onNextStateClicked() {
    const { changeTrafficState } = this.props
    let { currentIndex, actions } = this.state

    currentIndex++

    if (currentIndex === actions.length) {
      currentIndex = 0
    }

    const currentAction = actions[currentIndex]

    this.setState({ currentIndex, currentAction })
    changeTrafficState(currentAction)
  }

  render() {
    const { currentAction } = this.state
    return (
      <StateSwitcherComponent
        currentAction={currentAction}
        onNextStateClicked={() => {
          this.onNextStateClicked()
        }}
      />
    )
  }
}

StateSwitcherContainer.propTypes = {
  changeTrafficState: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => {
  return {
    changeTrafficState: type => {
      dispatch({ type })
    }
  }
}

// Функция connect оборачивает в себя React компоненту
// пробрасывая в props функцию (или несколько), которая диспатчит экшены (см. mapDispatchToProps)
export default connect(null, mapDispatchToProps)(StateSwitcherContainer)
