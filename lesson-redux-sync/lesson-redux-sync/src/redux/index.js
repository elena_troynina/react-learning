import { combineReducers, createStore } from 'redux'
import carTrafficLight from './reducers/CarTrafficLightReducer'

// Эта функция собирает все ваши редьюсеры в один (rootReducer)
// Когда появится надобность добавить еще один редьюсер,
// просто передавайте его следующим после carTrafficLight
export default () => {
  const rootReducer = combineReducers({
    carTrafficLight
  })

  // Возвращает функцию createStore,
  // которая вызывется в App.js
  return createStore(rootReducer)
}
