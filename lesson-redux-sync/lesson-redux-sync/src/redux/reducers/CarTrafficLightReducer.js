import * as Actions from '../../constants/Actions'
import { ON, OFF, BLINKING } from '../../constants/LightMode'

const initialState = {
  lights: [OFF, OFF, OFF],
  colors: ['red', 'yellow', 'green']
}

const carTrafficLightReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.TRAFFIC_DISALLOWED:
      return { ...state, lights: [ON, OFF, OFF] }

    case Actions.TRAFFIC_BEFORE_ALLOWED:
      return { ...state, lights: [ON, ON, OFF] }

    case Actions.TRAFFIC_ALLOWED:
      return { ...state, lights: [OFF, OFF, ON] }

    case Actions.TRAFFIC_AFTER_ALLOWED:
      return { ...state, lights: [OFF, OFF, BLINKING] }

    case Actions.TRAFFIC_BEFORE_DISALLOWED:
      return { ...state, lights: [OFF, ON, OFF] }

    case Actions.TRAFFIC_OUT_OF_SERVICE:
      return { ...state, lights: [OFF, BLINKING, OFF] }

    default:
      return state
  }
}

export default carTrafficLightReducer
