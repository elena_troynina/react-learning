import React, { Component } from 'react'
import { Provider } from 'react-redux'
import createStore from './redux'
import TrafficLightContainer from './containers/TrafficLightContainer'
import StateSwitcherContaner from './containers/StateSwitcherContainer'
import './App.css'

// Создаем стор для провайдера
const store = createStore()

class App extends Component {
  render() {
    return (
      // Оборачиваем приложение в redux
      <Provider store={store}>
        <div className="App">
          <TrafficLightContainer />
          <StateSwitcherContaner />
        </div>
      </Provider>
    )
  }
}

export default App
